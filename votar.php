<?php 
include_once("cabecario.php");
include_once("processa/verifica_se_tem_candidato.php");
include_once("./adm/conexao.php");
include_once($_SERVER['DOCUMENT_ROOT'] . '/' . $pasta . '/processa/seguranca.php');



//unset($_SESSION['dadosVotos']);



if(isset($_POST['tipoCandidato'],$_POST['action'])){
	
	
	if(!isset($_SESSION['dadosVotos']['tipoCandidatoJaVotado'])){
		$_SESSION['dadosVotos']['tipoCandidatoJaVotado'] = $_POST['tipoCandidato'].',';
	}else {
		$_SESSION['dadosVotos']['tipoCandidatoJaVotado'] .= $_POST['tipoCandidato'].',';
	}

	if($_POST['action'] == 'votar'){
		if(!isset($_SESSION['dadosVotos']['id_candidatos_votados'])){
			$_SESSION['dadosVotos']['id_candidatos_votados'][] = $_POST['idCandidato'];

			$array_candidatos = $_SESSION['dadosVotos']['id_candidatos_votados'];
			$array_candidatos = array_unique($array_candidatos);
			$_SESSION['dadosVotos']['id_candidatos_votados'] = array_values($array_candidatos);

		} else {
			$_SESSION['dadosVotos']['id_candidatos_votados'][] = $_POST['idCandidato'];
		}
	
		

	} elseif($_POST['action'] == 'nulo'){
		if(!isset($_SESSION['dadosVotos']['id_candidatos_nulos'])){
			$_SESSION['dadosVotos']['id_candidatos_nulos'][] = $_POST['idCandidato'];
		
			$array_candidatos = $_SESSION['dadosVotos']['id_candidatos_nulos'];
			$array_candidatos = array_unique($array_candidatos);
			$_SESSION['dadosVotos']['id_candidatos_votados'] = array_values($array_candidatos);

		} else {
			$_SESSION['dadosVotos']['id_candidatos_nulos'][] = $_POST['idCandidato'];
		}
	}

}

/*
echo '<pre>';
var_dump($_POST);
var_dump($_SESSION);
echo '</pre>';
*/

$jaVotados = '';
if(isset($_SESSION['dadosVotos']['tipoCandidatoJaVotado'])){
	$jaVotados = $_SESSION['dadosVotos']['tipoCandidatoJaVotado'];
	$jaVotados = substr($jaVotados,0,-1);
	$jaVotados = explode(',',$jaVotados);
	$jaVotados = "'".implode("','", $jaVotados)."'";
	$jaVotados = 'and t.nome not in ('.$jaVotados.')';
}

$tipo_candidato = mysql_query("SELECT *, t.nome as nome_tipo_candidato, t.id as id_candidato FROM tipo_candidato t
 where t.status = 'a' ".$jaVotados." limit 1");

$linhasTipoCandidato = mysql_num_rows($tipo_candidato);


$tipo_candidato = mysql_fetch_array($tipo_candidato);

if(isset($_POST['numero_candidato'], $_POST['id_tipo_candidato'])){
	$candidato = mysql_query("
		SELECT c.*, tc.nome as nome_tipo_candidato, p.nome as nome_partido, tc.possui_vice
		FROM `candidatos` c
		inner join tipo_candidato tc on (c.id_tipo_candidato = tc.id)
		inner join partidos p on (c.id_partido = p.id)
		where `numero` = {$_POST['numero_candidato']} and `id_tipo_candidato` = {$_POST['id_tipo_candidato']}
	");
	$linhasCandidato = mysql_num_rows($candidato);
	$candidato = mysql_fetch_array($candidato);
}

$dadosUsuario = mysql_query('
		SELECT * FROM pessoas where id = '.$_SESSION['pessoaId']);	
		$usuario = mysql_fetch_array($dadosUsuario);


?>


<script>
	function votoNulo(){
		document.getElementById('action'). value = 'nulo'
	}
</script>

<section>


	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	 crossorigin="anonymous">

<form method='post'>
	<div style='width:100%; margin-top:30px;'>
		<div class=" col-lg-4" style='margin: 0 auto;'>
		
		



		<?php
		if($usuario['ja_votou'] == 's') :

			echo 'aki';
			header('Location: votacaoFinalizada.php');
		elseif(isset($candidato)  && $linhasCandidato  > 0):?>

				<div class='item'>
					<div class="card">
						<div class="card-body text-center">
							<h2 class='text-center'>
								<?= $tipo_candidato['nome_tipo_candidato'] ?>
							</h2>
							<p class="d-flex align-items-end justify-content-center">
									<img class=" img-fluid mr-2" src="<?= './adm/foto/'.$candidato['foto']?>" style='max-width:200px;'>
									<img class=" img-fluid" src="<?= './adm/foto/'.$candidato['foto_vice']?>" style='max-width:100px;'>		
							</p>
							<h4 class="card-title">
								
								
								<?php
								if($candidato['possui_vice'] == 'S'){
									echo $candidato['nome']; echo ' e vice '.$candidato['nome_vice'];
								} else {
									echo $candidato['nome'];
								} 
								
								?>
							
							</h4>
							<div class="row">
								<p class="col-lg-12"><b>Número: </b>
								<?= $candidato['numero'] ?>
								</p>
							</div>
							<div class="row">
								<p class="col-lg-12"><b>Partido: </b>
								<?= $candidato['nome_partido'] ?>
								</p>
							</div>
							<div class="row">
								<p class="col-lg-12"><b>Vice: </b>
								<?= $candidato['nome_vice'] ?>
								</p>
							</div>
							<input type="hidden" id='action' name="action" value="votar">
							<input type="hidden" name="idCandidato" value="<?=$candidato['id']?>">
							<input type="hidden" id='tipoCandidato' name="tipoCandidato" value="<?=$candidato['nome_tipo_candidato']?>" >
							<div class="mt-2 d-flex justify-content-center">
								<input type="submit" class="btn btn-light col-lg-4" onclick="votoNulo()" style="background-color: #eff5fb" value='Nulo'>
								<a href='votar.php' class="btn btn-warning col-lg-4 ml-2">Corrigir</a>
								<input type="submit" class="btn btn-success col-lg-4 ml-2" value="Confirmar">
							</div>
						</div>
					</div>
				</div>
			
			</div>
			
						
			<?php
			
	
	elseif(isset($candidato) && $linhasCandidato  == 0):?>


		<div class='item'>

		<div class="card">
			<div class="alert alert-danger" role="alert">
			 Candidato inexistente
			</div>
			<div class="card-body text-center">
			<h2>Buscar candidato para <?= $tipo_candidato['nome_tipo_candidato'] ?></h2>
				<label>
				Digite o numero do seu candidato:
				</label>
				<input type="hidden" name='id_tipo_candidato' value='<?= $tipo_candidato['id_candidato'] ?>'>
				<input type="number" required  name='numero_candidato'>
				<div class='d-flex justify-content-center	'>
					<input type="submit" value='Buscar' class='btn btn-primary'>
				</div>
				
			</div>
		</div>
		</div>

		</div>



	<?php elseif($linhasTipoCandidato > 0) :?>

	<div class='item'>
		<div class="card">
			<div class="card-body text-center">
			<h2>Buscar candidato para <?= $tipo_candidato['nome_tipo_candidato'] ?></h2>
				<label>
				
				</label>
				<input type="hidden" name='id_tipo_candidato' value='<?= $tipo_candidato['id_candidato'] ?>'>

				<div class="form-group ">
					<label >Digite o numero do seu candidato:</label>
					<div class='row d-flex justify-content-center'>
					<input type="number" class="form-control col-lg-5"   name='numero_candidato'>
					</div>
				</div>

				
				<div class='d-flex justify-content-center	'>
					<input type="submit" value='Buscar' class='btn btn-primary'>
				</div>
			</div>
		</div>
		</div>
		</div>

	<?php elseif($linhasTipoCandidato == 0 && isset($_GET['action']) && $_GET['action'] == 'finalizarVotacao') :
	
	if(isset($_SESSION['dadosVotos']['id_candidatos_votados'])){
		$candidatos_votados = $_SESSION['dadosVotos']['id_candidatos_votados'];
		$id_candidatos_votados =  array_values(array_unique($candidatos_votados));
		foreach($id_candidatos_votados as $candidato){
			$verifica_se_tem_voto = mysql_query('SELECT * FROM `votos` WHERE `idCandidato` = '.$candidato);
			
			if(mysql_num_rows($verifica_se_tem_voto) > 0){

				$verifica_se_tem_voto = mysql_fetch_array($verifica_se_tem_voto);

				$qtdVoto = intval($verifica_se_tem_voto['qtdVoto'])+ 1;
				mysql_query('UPDATE `votos` SET `qtdVoto`= '.$qtdVoto.' WHERE `idCandidato` = '.$candidato);
				
			//	echo 'UPDATE `votos` SET `qtdVoto`= '.$qtdVoto.' WHERE `idCandidato` = '.$candidato;
			} else {
				mysql_query('INSERT INTO `votos`(`idCandidato`, `qtdVoto`, `qtdVotosNulos`) VALUES ('.$candidato.',1,0);');

			//	echo 'INSERT INTO `votos`(`idCandidato`, `qtdVoto`, `qtdVotosNulos`) VALUES ('.$candidato.',1,0);';

			}

		}
	}


	if(isset($_SESSION['dadosVotos']['id_candidatos_nulos'])){
		$candidatos_nulos = $_SESSION['dadosVotos']['id_candidatos_nulos'];
		$id_candidatos_nulos =  array_values(array_unique($candidatos_nulos));
		foreach($id_candidatos_nulos as $candidato){
			$verifica_se_tem_voto = mysql_query('SELECT * FROM `votos` WHERE `idCandidato` = '.$candidato);
			
			if(mysql_num_rows($verifica_se_tem_voto) > 0){

				$verifica_se_tem_voto = mysql_fetch_array($verifica_se_tem_voto);

				$qtdVoto = intval($verifica_se_tem_voto['qtdVotosNulos'])+ 1;
				
				mysql_query('UPDATE `votos` SET `qtdVotosNulos`= '.$qtdVoto.' WHERE `idCandidato` = '.$candidato);
				
			//	echo 'UPDATE `votos` SET `qtdVotosNulos`= '.$qtdVoto.' WHERE `idCandidato` = '.$candidato;

			} else {
			//	echo 'INSERT INTO `votos`(`idCandidato`, `qtdVoto`, `qtdVotosNulos`)  VALUES ('.$candidato.',0,1);';
			
			mysql_query('INSERT INTO `votos`(`idCandidato`, `qtdVoto`, `qtdVotosNulos`) VALUES ('.$candidato.',0,1);');
			}

		}
	}
	mysql_query('UPDATE pessoas set ja_votou = \'s\' where id = '.$_SESSION['pessoaId']);

	//echo 'UPDATE pessoas set ja_votou = \'s\' where id = '.$_SESSION['pessoaId'];
	header('Location: votacaoFinalizada.php');
	?>


	<?php elseif($linhasTipoCandidato == 0) :
	 $candidatos_votados = $_SESSION['dadosVotos']['id_candidatos_votados'];

	 $id_candidatos_votados =  array_values(array_unique($candidatos_votados));
	 $id_candidatos_votados = implode(',',$id_candidatos_votados);

	$candidatos_votados = mysql_query('SELECT c.*, tc.nome as nome_tipo_candidato, p.nome as nome_partido, tc.possui_vice
	FROM `candidatos` c
	inner join tipo_candidato tc on (c.id_tipo_candidato = tc.id)
	inner join partidos p on (c.id_partido = p.id)
	where c.`id` in ('.$id_candidatos_votados.')');

	?>

		<div style='background-color:white;border-radius: 5px;' class='mb-5 pb-3'>
			<h2 class='text-center' >Dados da votação</h2>
			<table class="table">
				<thead>
				  <tr>
					<th scope="col">numero</th>
					<th scope="col">Candidato</th>
					<th scope="col">vice</th>
					<th scope="col">partido</th>
					<th scope="col">Tipo Candidato</th>
				  </tr>
				</thead>
				<tbody>
				<?php while($candidato = mysql_fetch_array($candidatos_votados)) :?>
					<tr>
						<td scope="row"><?= $candidato['numero'];?></td>
						<td><?= $candidato['nome'];?></td>
						<td><?= $candidato['nome_vice'];?></td>
						<td><?= $candidato['nome_partido'];?></td>
						<td><?= $candidato['nome_tipo_candidato'];?></td>
					</tr>
				<?php endwhile;?>
				<?php 
				
				if(isset($_SESSION['dadosVotos']['id_candidatos_nulos'])){
					$id_candidatos_nulos = $_SESSION['dadosVotos']['id_candidatos_nulos'];

					$id_candidatos_nulos =  array_values(array_unique($id_candidatos_nulos));
					$id_candidatos_nulos = implode(',',$id_candidatos_nulos);

					$nulos = mysql_query('SELECT c.*, tc.nome as nome_tipo_candidato, p.nome as nome_partido, tc.possui_vice
					FROM `candidatos` c
					inner join tipo_candidato tc on (c.id_tipo_candidato = tc.id)
					inner join partidos p on (c.id_partido = p.id)
					where c.`id` in ('.$id_candidatos_nulos.')');
					
					while($nulo = mysql_fetch_array($nulos)) :?>
						<tr>
							<td scope="row" class='text-center' colspan='4'>Voto Nulo</td>
							
							<td><?= $nulo['nome_tipo_candidato'];?></td>
						</tr>
					<?php endwhile;
				}
				?>
				</tbody>
			  </table>
			  <div class='d-flex justify-content-center mt-5'>
				  <a href="votar.php?action=finalizarVotacao" class='btn btn-primary' style='cursor:pointer;'>Finalizar</a>
			  </div>
		</div>



	<?php endif;?>


	</div>
	</form>
</section>

<?php include_once("rodape.php"); ?>