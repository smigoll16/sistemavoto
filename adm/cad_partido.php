<?php
  include_once("conexao.php");
  include_once("cabecario.php");
  include_once("menu.php");

    if(isset($_POST['acao']) && $_POST['acao'] == 'criar'){
        $resultado = mysql_query('INSERT INTO partidos (nome, descricao, status)
       values  (
       \''.$_POST['nome'].'\',
       \''.$_POST['descricao'].'\',
       \''.$_POST['status'].'\'
       );');

        if(mysql_affected_rows() == 1){
            echo "<script>alert('Partido cadastrado com sucesso!!')</script>";
        } else  {
            echo "<script>alert('Erro ao tentar cadastrar um partido!!')</script>";
        }
    }
    if(isset($_GET['acao'], $_POST['id']) && $_GET['acao'] == 'alterar'){   
    $resultado = mysql_query('UPDATE partidos
        SET nome = \''.$_POST['nome'].'\',
        descricao = \''.$_POST['descricao'].'\',
        status = \''.$_POST['status'].'\'
        where id = '.$_POST['id']);

        if(mysql_affected_rows() == 1){
            echo "<script>alert('Cadastro alterado com sucesso!!')</script>";
        } else  {
            echo "<script>alert('Erro ao tentar alterar cadastro!!')</script>";
        }
    }
    if(isset($_GET['acao']) && $_GET['acao'] == 'remover'){
        $hasCandidato = mysql_query("SELECT * FROM candidatos WHERE id_partido =".$_GET['id']);
        if(mysql_affected_rows() == 0){
            $resultado = mysql_query("DELETE FROM partidos WHERE id = ".$_GET['id']);
            if(mysql_affected_rows() == 1){
                echo "<script>alert('Partido removido com sucesso!!')</script>";
            } else  {
                echo "<script>alert('Erro ao tentar remover o partido!!')</script>";
            }
        } else {
            echo "<script>alert('Não pode remover uma partido que tem candidato!!')</script>";
        }

    }
	
	$resultado=mysql_query("SELECT * FROM partidos");
	$linhas=mysql_num_rows($resultado);
	$contr_sob = $linhas;
?>
<div class="container theme-showcase mt-4" style="background-color:white;border-radius: 5px;" role="main">
    <div class="page-header">
        <h1>Lista de Partidos</h1>
    </div>
    <div class="row float-right mr-3">
        <div>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cadastrar">Cadastrar</button>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Dercrição</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
				$contr_ord = 1;
				while($linhas = mysql_fetch_array($resultado)){					
					echo "<tr>";
						echo "<td>".$linhas['id']."</td>";
						echo "<td>".$linhas['nome']."</td>";
                        echo "<td>".$linhas['descricao']."</td>";
                        echo "<td>". ($linhas['status'] == 'a' ? 'Ativo' : 'Desativado') ."</td>";
                        
						?>
                    <td>
                        <button type='button' data-toggle="modal" data-target="#detalhes<?=$linhas['id']?>" class='btn btn-sm btn-primary'>Detalhes</button>
                        <button type='button' data-toggle="modal" data-target="#editar<?=$linhas['id']?>" class='btn btn-sm btn-warning'>Editar</button>
                        <a href='<?=$_SERVER['PHP_SELF']?>?acao=remover&id=<?=$linhas['id']?>'>
                            <button type='button' class='btn btn-sm btn-danger'>Apagar</button>
                        </a>
                    </td>
                    </tr>

                    <!-- Modal -->
                    <div class="modal fade" id="detalhes<?=$linhas['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Detalhes</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <p class="col-lg-12"><b>Nome: </b>
                                            <?= $linhas['nome']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Descrição: </b>
                                            <?= $linhas['descricao']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Status: </b>
                                            <?=($linhas['status'] == 'a' ? 'Ativo' : 'Desativado')?>
                                        </p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editar<?=$linhas['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Alterar Cadastro</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" method='POST' action="<?=$_SERVER['PHP_SELF']?>?acao=alterar">
                                        <input type="hidden" class="form-control" name='acao' value='alterar'>
                                        <input type="hidden" class="form-control" name='id' value='<?= $linhas['id']?>'>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Nome:</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value='<?= $linhas['nome']?>' class="form-control" name='nome'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Descrição:</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value='<?= $linhas['descricao']?>' class="form-control" name='descricao'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Status:</label>
                                            <div class="col-sm-10">
                                                <select required name="status" value='<?= $linhas['status']?>' class="form-control">
                                                    <option <?= $linhas['status'] == 'a' ? 'selected': '' ?> value="a">Ativo</option>
                                                    <option <?= $linhas['status'] == 'd' ? 'selected': '' ?> value="d">Desativo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                                            <input type="submit" class="btn btn-primary" value='Editar'>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>





                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cadastrar Partido</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method='POST' id='forms' action="<?=$_SERVER['PHP_SELF']?>?acao=adicionar">
                    <input type="hidden" class="form-control" name='acao' value='criar'>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nome:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='nome'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descrição:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='descricao'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Status:</label>
                        <div class="col-sm-10">
                            <select required name="status" class="form-control">
                                <option value="a">Ativo</option>
                                <option value="d">Desativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                        <input type="submit" class="btn btn-primary" value='Criar'>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<?php include_once("rodape.php");?>
<!-- /container -->