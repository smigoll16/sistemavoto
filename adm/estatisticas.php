<?php
  include_once("conexao.php");
  include_once("cabecario.php");
  include_once("menu.php");
  
  
  $tipos_candidato=mysql_query("SELECT * FROM tipo_candidato");

if(isset($_GET['tipoCandidato'])){
    $tipo_selecionado = mysql_query("SELECT * FROM tipo_candidato where id = ".$_GET['tipoCandidato']);
    $nome_tipo_selectionado = mysql_fetch_array($tipo_selecionado)['nome'];
}else {

    $tipo_selecionado = mysql_query("SELECT * FROM tipo_candidato");
    $_GET['tipoCandidato'] = mysql_fetch_array($tipo_selecionado)['id'];
    $nome_tipo_selectionado = mysql_fetch_array($tipo_selecionado)['nome'];


}


  ?>

    <section>

    <div class="container theme-showcase mt-4" style="background-color:white;border-radius: 5px;" role="main">
      <div class="row pt-1" style="text-align:center !important;display:block;">
        <div class="mt-4 ml-4">
            <?php while($tipo = mysql_fetch_array($tipos_candidato)){ ?>
          <a class="btn btn-info btn-sm" href='estatisticas.php?tipoCandidato=<?= $tipo['id']?>'><?=$tipo['nome']?></a>
            <?php }?>
        </div>    
      </div>
      <div class="page-header mt-2">
          <h1>Estatisticas de Votos<?= isset($nome_tipo_selectionado)?': '.$nome_tipo_selectionado:'';?></h1>
      </div>
    <div>    
        <canvas id="myChart" ></canvas>
      </div>
    <div>
    

<?php

$votos = mysql_query('SELECT c.nome as nome_candidato, t.nome as nome_tipo_candidato, COALESCE(sum(v.qtdVoto),0) as total_voto
FROM candidatos c
left join tipo_candidato t on (c.id_tipo_candidato = t.id)
left join votos v on (c.id = v.idCandidato)
WHERE t.id = '.$_GET['tipoCandidato'].'
group by c.nome, t.nome');

$labels = '';
$totalVotos = '';

$colorGraphic = array(
   (object)[ 'cor' => 'rgba(255, 99, 132, 0.2)',
    'borda' => 'rgba(255,99,132,1)'],
    (object)[ 'cor' => 'rgba(54, 162, 235, 0.2)',
    'borda' => 'rgba(54, 162, 235, 1)'],
    (object)[ 'cor' => 'rgba(255, 206, 86, 0.2)',
    'borda' => 'rgba(255, 206, 86, 1)'],
    (object)[ 'cor' => 'rgba(75, 192, 192, 0.2)',
    'borda' => 'rgba(75, 192, 192, 1)']
    
);

$cor = '';
$borda = '';

while($voto = mysql_fetch_array($votos)){
    $index = rand(0,3);
    $cor .= '\''.$colorGraphic[$index]->cor.'\',';
    $borda .= '\''.$colorGraphic[$index]->borda.'\',';


    $labels .= '\''.$voto['nome_candidato'].'\',';
    $totalVotos .= $voto['total_voto'].',';
}

$totalNulo = mysql_query('SELECT COALESCE(sum(v.qtdVotosNulos),0) as total_nulo
FROM candidatos c
inner join tipo_candidato t on (c.id_tipo_candidato = t.id)
inner join votos v on (c.id = v.idCandidato)
WHERE t.id = '.$_GET['tipoCandidato']);

$totalNulo = mysql_fetch_array($totalNulo);
$totalNulo = $totalNulo['total_nulo'];
$labels .= '\'Nulos\',';
$totalVotos .= $totalNulo.',';
$borda .= '\'rgba(2, 0, 0, 0.5);\',';
$cor = '\'rgba(215, 196, 228, 0.6);\',';


$labels = substr( $labels,0,-1);
$totalVotos = substr( $totalVotos,0,-1);
$borda = substr( $borda,0,-1);
$cor = substr( $cor,0,-1);


?>


    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: [<?= $labels?>],
              datasets: [{
                  label: [<?= $labels?>],
                  data: [<?= $totalVotos?>],
                  backgroundColor: [
                     <?= $cor?>
                  ],
                  borderColor: [
                    <?= $borda?>
                  ],
                  borderWidth: 2
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
      });
</script>
  <?php include_once("rodape.php")?>