<?php
  include_once("conexao.php");
  include_once("cabecario.php");
  include_once("menu.php");

  $path = './foto/';

    if(isset($_POST['acao']) && $_POST['acao'] == 'criar'){ 
        
        $pathFoto = time().'candidato.'.explode('.',$_FILES['foto']['name'])[1];
        $pathFotoVice = time().'vicecandidato.'.explode('.',$_FILES['fotoVice']['name'])[1];

        if(move_uploaded_file($_FILES['foto']['tmp_name'],$path.$pathFoto) && move_uploaded_file($_FILES['fotoVice']['tmp_name'],$path.$pathFotoVice)){

            $resultado = mysql_query('INSERT INTO candidatos (nome, numero, foto, id_partido, id_tipo_candidato, nome_vice, foto_vice)
        values  (
        \''.$_POST['nome'].'\',
        \''.$_POST['numero'].'\',
        \''.$pathFoto.'\',
        '.$_POST['partido'].',
        '.$_POST['tipo'].',
        \''.$_POST['nomevice'].'\',
        \''.$pathFotoVice.'\'
        );');

            if(mysql_affected_rows() == 1){
                echo "<script>alert('Candidato cadastrado com sucesso!!')</script>";
            } else  {
                echo "<script>alert('Erro ao tentar cadastrar um candidato!!')</script>";
                unlink($path.$pathFoto);
                unlink($path.$pathFotoVice);
            }
        } else {
            echo "<script>alert('Erro ao tentar cadastrar um candidato!!')</script>";
        }
    }

    if(isset($_GET['acao'], $_POST['id_candidato']) && $_GET['acao'] == 'alterar'){   

        if(!empty($_FILES['foto']['name'])){
            $pathFoto = time().'candidato.'.explode('.',$_FILES['foto']['name'])[1];
            $pathFotoVice = time().'vicecandidato.'.explode('.',$_FILES['fotoVice']['name'])[1];


            if(move_uploaded_file($_FILES['foto']['tmp_name'],$path.$pathFoto) && move_uploaded_file($_FILES['fotoVice']['tmp_name'],$path.$pathFotoVice)){
                unlink($path.$_POST['linkFotoAntiga']);
                unlink($path.$_POST['linkFotoAntigaVice']);
            } else {
                echo "<script>alert('Erro ao tentar alterar foto!!')</script>";
            }
        }

    $pathFoto = (!empty($pathFoto))? 'foto = \''.$pathFoto.'\',' :'';
    $pathFotoVice = (!empty($pathFoto))? ',foto_vice = \''.$pathFotoVice.'\'' :'';

    $resultado = mysql_query('UPDATE candidatos
        set nome =   \''.$_POST['nome'].'\',
        numero = '.$_POST['numero'].',
        '.$pathFoto.'
        id_partido =  '.$_POST['partido'].',
        id_tipo_candidato =  '.$_POST['tipo'].',
         nome_vice = \''.$_POST['nome_vice'].'\'
         '.$pathFotoVice.'
           where  id = '.$_POST['id_candidato']);

         
        if(mysql_affected_rows() == 1){
            echo "<script>alert('Cadastro alterado com sucesso!!')</script>";
        } else  {
            echo "<script>alert('Erro ao tentar alterar cadastro!!')</script>";
        }
    }
    if(isset($_GET['acao']) && $_GET['acao'] == 'remover'){
        $resultado = mysql_query("DELETE FROM candidatos WHERE id = ".$_GET['id']);
        $linhas = mysql_fetch_array($resultado);
        if(mysql_affected_rows() == 1){
            echo "<script>alert('Candidato removido com sucesso!!')</script>";
            unlink($path.$linhas['foto']);
            unlink($path.$linhas['foto_vice']);
        } else  {
            echo "<script>alert('Erro ao tentar remover o candidato!!')</script>";
        }
    }
	
    $resultado=mysql_query("SELECT *, c.id as idCandidato,t.possui_vice, t.nome as nome_tipo, p.nome as nome_partido, c.nome as nome_candidato FROM candidatos AS c INNER JOIN partidos p ON (p.id = id_partido) INNER JOIN tipo_candidato t ON (t.id = id_tipo_candidato)");
    $tipo_candidato=mysql_query("SELECT * FROM tipo_candidato");
    $partidos=mysql_query("SELECT * FROM partidos");

    $opPartidos = '';
    while($partido = mysql_fetch_array($partidos)){	
        $opPartidos .= "<option value=".$partido['id'].">".$partido['nome']."</option>";
    }


    $tipo_candidato=mysql_query("SELECT * FROM tipo_candidato");
    $opTipoCandidatos = '';
    while($tipo = mysql_fetch_array($tipo_candidato)){	
        $opTipoCandidatos .= "<option value=".$tipo['id'].">".$tipo['nome']."</option>";
    }

	$linhas=mysql_num_rows($resultado);
	$contr_sob = $linhas;
?>
<div class="container theme-showcase mt-4" style="background-color:white;border-radius: 5px;" role="main">
    <div class="page-header">
        <h1>Lista de Candidatos</h1>
    </div>
    <div class="row float-right mr-3">
        <div>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cadastrar">Cadastrar</button>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Número</th>
                        <th>Nome</th>
                        <th>Cargo</th>
                        <th>Partido</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
				$contr_ord = 1;
				while($linhas = mysql_fetch_array($resultado)){					
					echo "<tr>";
						echo "<td>".$linhas['idCandidato']."</td>";
                        echo "<td>".$linhas['numero']."</td>";
                        echo "<td>".$linhas['nome_candidato']."</td>";
                        echo "<td>".$linhas['nome_tipo']."</td>";
                        echo "<td>".$linhas['nome_partido']."</td>";
                        
						?>
                    <td>
                        <button type='button' data-toggle="modal" data-target="#detalhes<?=$linhas['idCandidato']?>" class='btn btn-sm btn-primary'>Detalhes</button>
                        <button type='button' data-toggle="modal" data-target="#editar<?=$linhas['idCandidato']?>" class='btn btn-sm btn-warning'>Editar</button>
                        <a href='<?=$_SERVER['PHP_SELF']?>?acao=remover&id=<?=$linhas['idCandidato']?>'>
                            <button type='button' class='btn btn-sm btn-danger'>Apagar</button>
                        </a>
                    </td>
                    </tr>

                    <!-- Modal -->
                    <div class="modal fade" id="detalhes<?=$linhas['idCandidato']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Detalhes</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row card-body text-center">
                                        <img class="img-fluid" src="./foto/<?=$linhas['foto']?>" style='max-width:200px;max-height:200px;'>
                                        <img class="img-fluid ml-2" src="./foto/<?=$linhas['foto_vice']?>" style='max-width:100px;max-height:100px; display: flex; align-self: flex-end;'>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Número: </b>
                                            <?= $linhas['numero']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Nome: </b>
                                            <?= $linhas['nome_candidato']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Partido: </b>
                                            <?= $linhas['nome_partido']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Cargo: </b>
                                            <?= $linhas['nome_tipo']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Vice: </b>
                                            <?= $linhas['nome_vice']?>
                                        </p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editar<?=$linhas['idCandidato']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Alterar Cadastro</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" enctype="multipart/form-data" method='POST' action="<?=$_SERVER['PHP_SELF']?>?acao=alterar">
                                        <input type="hidden" class="form-control" name='acao' value='alterar'>
                                        <input type="hidden" class="form-control" name='id_candidato' value='<?= $linhas['idCandidato']?>'>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Número:</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value='<?= $linhas['numero']?>' class="form-control" name='numero'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Nome:</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value="<?= $linhas['nome_candidato']?>" class="form-control" name='nome'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Partido:</label>
                                            <div class="col-sm-10">
                                                <select required name="partido" class="form-control">
                                                <?=  
                                                $opPartidos = '';

                                                $partidos_1=mysql_query("SELECT * FROM partidos");

                                                while($partido = mysql_fetch_array($partidos_1)){	
                                                    if($partido['id'] == $linhas['id_partido']) {
                                                        $selected = 'selected';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    $opPartidos .= "<option {$selected}  value=".$partido['id'].">".$partido['nome']."</option>";
                                                }

                                                echo  $opPartidos;

                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Cargo:</label>
                                            <div class="col-sm-10">
                                                <select required name="tipo" class="form-control">
                                                <?php
                                                
                                                                                                
                                                    $tipo_candidato_1=mysql_query("SELECT * FROM tipo_candidato");
                                                    $opTipoCandidatos = '';
                                                    while($tipo = mysql_fetch_array($tipo_candidato_1)){
                                                        if($tipo['id'] == $linhas['id_tipo_candidato']) {
                                                            $selected = 'selected';
                                                        } else {
                                                            $selected = '';
                                                        }
                                                        

                                                        $opTipoCandidatos .= "<option {$selected}  value=".$tipo['id'].">".$tipo['nome']."</option>";
                                                    }
                                                
                                                    echo  $opTipoCandidatos ?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php if($linhas['possui_vice'] == 'S') :?>

                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Vice:</label>
                                                    <div class="col-sm-10">
                                                    <input required type="text" value="<?= $linhas['nome_vice']?>" class="form-control" name='nome_vice'>
                                                    </div>
                                                </div>

                                                <div class="form-group mt-2">
                                                    <label class="col-sm-10 control-label">Foto vice(500x500):</label>
                                                    <div class="col-sm-10">
                                                    <input type="file" class="form-control" name="fotoVice">
                                                    </div>
                                                </div>

                                                <div class="col mt-2">
                                                    <p><b>Foto Atual:</b></p>
                                                    <input type="hidden" name='linkFotoAntigaVice' value='<?=$linhas['foto_vice']?>'>
                                                <center><img src="./foto/<?=$linhas['foto_vice']?>" class="img-fluid" style='max-width:200px;' alt="fotoAtual"></center>
                                                </div>
                                        <?php endif;?>

                                          <div>
                                            <label class="col-sm-10 control-label">Foto candidato(500x500):</label>
                                            <div class="col-sm-10">
                                            <input type="file" class="form-control" name="foto">
                                            </div>
                                        </div>
                                        <div class="col mt-2">
                                            <p><b>Foto Atual:</b></p>
                                        
                                            <input type="hidden" name='linkFotoAntiga' value='<?=$linhas['foto']?>'>
                                          <center><img src="./foto/<?=$linhas['foto']?>" class="img-fluid" style='max-width:200px; ' alt="fotoAtual"></center>
                                        </div> 



                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                                            <input type="submit" class="btn btn-primary" value='Editar'>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>





                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cadastrar Candidato</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" enctype="multipart/form-data" method='POST' id='forms' action="<?=$_SERVER['PHP_SELF']?>?acao=adicionar">
                    <input type="hidden" class="form-control" name='acao' value='criar'>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nome:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='nome'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Número:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='numero'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Partido:</label>
                        <div class="col-sm-10">
                            <select required name="partido" class="form-control">
                            <?=  $opPartidos ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Cargo:</label>
                        <div class="col-sm-10">
                            <select required name="tipo" class="form-control">
                            <?= $opTipoCandidatos ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Nome Vice:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='nomevice'>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-10 control-label">Foto candidato(500x500):</label>
                        <div class="col-sm-10">
                          <input type="file" required class="form-control" name="foto">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-10 control-label">Foto vice(500x500):</label>
                        <div class="col-sm-10">
                          <input type="file" required class="form-control" name="fotoVice">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                        <input type="submit" class="btn btn-primary" value='Criar'>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<?php include_once("rodape.php");?>
<!-- /container -->