<?php
session_start();
session_destroy();
unset($_SESSION['pessoaId']);
unset($_SESSION['pessoaNome']);
unset($_SESSION['pessoaTitulo']);
unset($_SESSION['pessoaSenha']);
unset($_SESSION['tipoUsuario']);
header("Location: ../admLogin.php");
?>
