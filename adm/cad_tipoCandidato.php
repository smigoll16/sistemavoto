<?php
  include_once("conexao.php");
  include_once("cabecario.php");
  include_once("menu.php");

    if(isset($_POST['acao']) && $_POST['acao'] == 'criar'){
        $resultado = mysql_query('INSERT INTO tipo_candidato (nome, status,possui_vice)
       values  (
       \''.$_POST['nome'].'\',
       \''.$_POST['status'].'\'
        \''.$_POST['possui_vice'].'\'
       );');

        if(mysql_affected_rows() == 1){
            echo "<script>alert('Tipo de candidato cadastrado com sucesso!!')</script>";
        } else  {
            echo "<script>alert('Erro ao tentar cadastrar um tipo de candidato!!')</script>";
        }
    }
    if(isset($_GET['acao'], $_POST['id']) && $_GET['acao'] == 'alterar'){   
    $resultado = mysql_query('UPDATE tipo_candidato
        SET nome = \''.$_POST['nome'].'\',
        status = \''.$_POST['status'].'\',
        possui_vice = \''.$_POST['possui_vice'].'\'
        where id = '.$_POST['id']);

        if(mysql_affected_rows() == 1){
            echo "<script>alert('Cadastro alterado com sucesso!!')</script>";
        } else  {
            echo "<script>alert('Erro ao tentar alterar cadastro!!')</script>";
        }
    }
    if(isset($_GET['acao']) && $_GET['acao'] == 'remover'){
        $hasCandidato = mysql_query("SELECT * FROM candidatos WHERE id_tipo_candidato =".$_GET['id']);
        if(mysql_affected_rows() == 0){
            $resultado = mysql_query("DELETE FROM tipo_candidato WHERE id = ".$_GET['id']);
            if(mysql_affected_rows() == 1){
                echo "<script>alert('Tipo de Candidato removido com sucesso!!')</script>";
            } else  {
                echo "<script>alert('Erro ao tentar remover o tipo!!')</script>";
            }
        }else{
            echo "<script>alert('Não pode remover uma tipo que tem candidato!!')</script>";
        }
    }
	
	$resultado=mysql_query("SELECT * FROM tipo_candidato");
	$linhas=mysql_num_rows($resultado);
	$contr_sob = $linhas;
?>
<div class="container theme-showcase mt-4" style="background-color:white;border-radius: 5px;" role="main">
    <div class="page-header">
        <h1>Tipos de Candidatos</h1>
    </div>
    <div class="row float-right mr-3">
        <div>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cadastrar">Cadastrar</button>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
				$contr_ord = 1;
				while($linhas = mysql_fetch_array($resultado)){					
					echo "<tr>";
						echo "<td>".$linhas['id']."</td>";
						echo "<td>".$linhas['nome']."</td>";
                        echo "<td>". ($linhas['status'] == 'a' ? 'Ativo' : 'Desativado') ."</td>";
                        
						?>
                    <td>
                        <button type='button' data-toggle="modal" data-target="#detalhes<?=$linhas['id']?>" class='btn btn-sm btn-primary'>Detalhes</button>
                        <button type='button' data-toggle="modal" data-target="#editar<?=$linhas['id']?>" class='btn btn-sm btn-warning'>Editar</button>
                        <a href='<?=$_SERVER['PHP_SELF']?>?acao=remover&id=<?=$linhas['id']?>'>
                            <button type='button' class='btn btn-sm btn-danger'>Apagar</button>
                        </a>
                    </td>
                    </tr>

                    <!-- Modal -->
                    <div class="modal fade" id="detalhes<?=$linhas['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Detalhes</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <p class="col-lg-12"><b>Tipo: </b>
                                            <?= $linhas['nome']?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-12"><b>Status: </b>
                                            <?=($linhas['status'] == 'a' ? 'Ativo' : 'Desativado')?>
                                        </p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editar<?=$linhas['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Alterar Cadastro</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" method='POST' action="<?=$_SERVER['PHP_SELF']?>?acao=alterar">
                                        <input type="hidden" class="form-control" name='acao' value='alterar'>
                                        <input type="hidden" class="form-control" name='id' value='<?= $linhas['id']?>'>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Tipo:</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value='<?= $linhas['nome']?>' class="form-control" name='nome'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Status:</label>
                                            <div class="col-sm-10">
                                                <select required name="status" value='<?= $linhas['status']?>' class="form-control">
                                                    <option <?= $linhas['status'] == 'a' ? 'selected': '' ?> value="a">Ativo</option>
                                                    <option <?= $linhas['status'] == 'd' ? 'selected': '' ?> value="d">Inativo</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Possui Vice:</label>
                                            <div class="col-sm-10">
                                                <select required name="possui_vice" value='<?= $linhas['possui_vice']?>' class="form-control">
                                                    <option <?= $linhas['possui_vice'] == 'S' ? 'selected': '' ?> value="S">Sim</option>
                                                    <option <?= $linhas['possui_vice'] == 'N' ? 'selected': '' ?> value="N">Não</option>
                                                </select>
                                            </div>
                                        </div>
                                       

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                                            <input type="submit" class="btn btn-primary" value='Editar'>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>





                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cadastrar Tipo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method='POST' id='forms' action="<?=$_SERVER['PHP_SELF']?>?acao=adicionar">
                    <input type="hidden" class="form-control" name='acao' value='criar'>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Tipo:</label>
                        <div class="col-sm-10">
                            <input required type="text" class="form-control" name='nome'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-1 col-lg-offset-3 control-label">Status:</label>
                        <div class="col-sm-10">
                            <select required name="status" class="form-control">
                                <option value="a">Ativo</option>
                                <option value="d">Desativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">sair</button>
                        <input type="submit" class="btn btn-primary" value='Criar'>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<?php include_once("rodape.php");?>
<!-- /container -->