-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01-Out-2018 às 01:51
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistema_eleitoral`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `candidatos`
--

CREATE TABLE `candidatos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `id_partido` int(11) NOT NULL,
  `id_tipo_candidato` int(11) NOT NULL,
  `nome_vice` varchar(255) NOT NULL,
  `foto_vice` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `candidatos`
--

INSERT INTO `candidatos` (`id`, `nome`, `numero`, `foto`, `id_partido`, `id_tipo_candidato`, `nome_vice`, `foto_vice`) VALUES
(1, 'Lula Ladão', '13', '1538327061candidato.jpg', 1, 3, 'Outro Ladrão', '1538327061vicecandidato.jpg'),
(3, 'João Amoedo', '30', '1538327162candidato.jpg', 5, 3, 'Não sei', '1538327162vicecandidato.png'),
(4, 'tadeu', '456', 'f', 5, 4, 'vice', 'f'),
(5, 'teste 2', '458', 'f', 3, 4, 'jurineu', 'f'),
(6, 'derpino', '4566', 'f', 3, 3, 'obama', 'f'),
(7, 'ttetet', '875', 'f', 3, 5, 'poiu', 'f'),
(13, 'Bolsonaro', '17', '1537994679candidato.jpg', 1, 3, 'Mourão', '1537994679vicecandidato.jpg'),
(14, 'barack obama', '789', '1538349307candidato.jpg', 6, 5, 'trump', '1538349307vicecandidato.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `partidos`
--

CREATE TABLE `partidos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `partidos`
--

INSERT INTO `partidos` (`id`, `nome`, `descricao`, `status`) VALUES
(1, 'PT', 'Partido dos Trabaladores', 'a'),
(3, 'NOVO', 'Partido novo', 'a'),
(5, 'PC do B', 'Partido Comunista do Brasil', 'a'),
(6, 'PSL', 'Partido Social Liberal', 'a');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `cpf` varchar(50) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `status` char(1) NOT NULL,
  `ja_votou` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoas`
--

INSERT INTO `pessoas` (`id`, `nome`, `titulo`, `cpf`, `senha`, `tipo`, `status`, `ja_votou`) VALUES
(1, 'Marcos Vinicios Vanderlei', '123456789', '123456789', '123', 'Admin', 'n', 'n'),
(2, 'eleitor', '123456789', '1234567899', '123', 'Eleitor', 'n', ''),
(3, 'matheus', '456', '456', '456', 'Eleitor', 'n', 's'),
(4, 'jefferson', '159', '159', '159', 'Eleitor', 'n', 's'),
(5, 'jubileu', '753', '753', '753', 'Eleitor', 'n', 'n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_candidato`
--

CREATE TABLE `tipo_candidato` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `possui_vice` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo_candidato`
--

INSERT INTO `tipo_candidato` (`id`, `nome`, `status`, `possui_vice`) VALUES
(3, 'Prefeito', 'a', 'S'),
(4, 'Vereador', 'a', 'N'),
(5, 'Presidente', 'a', 'S');

-- --------------------------------------------------------

--
-- Estrutura da tabela `votos`
--

CREATE TABLE `votos` (
  `idVoto` int(11) NOT NULL,
  `idCandidato` int(11) NOT NULL,
  `qtdVoto` int(11) NOT NULL,
  `qtdVotosNulos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `votos`
--

INSERT INTO `votos` (`idVoto`, `idCandidato`, `qtdVoto`, `qtdVotosNulos`) VALUES
(12, 1, 2, 1),
(13, 4, 1, 0),
(14, 14, 1, 0),
(15, 7, 1, 0),
(16, 5, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidatos`
--
ALTER TABLE `candidatos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partidos`
--
ALTER TABLE `partidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_candidato`
--
ALTER TABLE `tipo_candidato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `votos`
--
ALTER TABLE `votos`
  ADD PRIMARY KEY (`idVoto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidatos`
--
ALTER TABLE `candidatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `partidos`
--
ALTER TABLE `partidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tipo_candidato`
--
ALTER TABLE `tipo_candidato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `votos`
--
ALTER TABLE `votos`
  MODIFY `idVoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
