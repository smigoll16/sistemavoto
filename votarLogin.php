<?php 
  include_once("adm/conexao.php");
  include_once("cabecario.php");
  

  session_destroy();
  unset($_SESSION['pessoaId']);
  unset($_SESSION['pessoaNome']);
  unset($_SESSION['pessoaTitulo']);
  unset($_SESSION['pessoaSenha']);
  unset($_SESSION['tipoUsuario']);


  if(isset($_SESSION['pessoaId'])){
    header('Location: votar.php');
  }
  ?>

<section>
  <div class="container background-white" style="max-width: 40%; height: 40%; border: 1px solid #b8daff; border-radius: .25rem; margin-top: 10%;">
    <div class="alert alert-primary" role="alert">
      Entrar
    </div>
    <?php
    if(isset($_GET['erroLogin'])){
      echo '<div class="alert alert-danger" role="alert">
      Usuario ou senha invalidos
    </div>';
    } elseif (isset($_GET['erroAuth'])){
      echo '<div class="alert alert-warning" role="alert">
        efetue o login
      </div>';
    }
    ?>

    <form action="processa/valida_login.php" method="POST">
      <div class="form-group mt-4">
        <label for="exampleInputEmail1">Título de Eleitor</label>
        <input required name="titulo" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Título de eleitor">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Senha</label>
        <input required name="senha" type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha">
      </div>
      <div class="form-group form-check">
      </div>
      <input type="submit" class="btn btn-primary mb-4" value="Logar"/>
    </form>
  </div>
</section>

<?php include_once("rodape.php");?>