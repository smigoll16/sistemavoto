
<?php
include_once("cabecario.php");

session_start();
session_destroy();
unset($_SESSION['pessoaId']);
unset($_SESSION['pessoaNome']);
unset($_SESSION['pessoaTitulo']);
unset($_SESSION['pessoaSenha']);
unset($_SESSION['tipoUsuario']);

include_once("rodape.php");?>

<div class='d-flex justify-content-center'>
<h3 class='mt-5 pt-5'>Votação concluida, aguarde o resultado.</h3>
</div>
